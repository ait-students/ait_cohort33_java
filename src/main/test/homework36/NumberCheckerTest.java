package homework36;

import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class NumberCheckerTest {

    private LogCaptor logCaptor = LogCaptor.forClass(NumberChecker.class);

    @Test
    void testValidNumberInLogger() {
        NumberChecker numberChecker = new NumberChecker();
        numberChecker.checkNumber("123");
        logCaptor.setLogLevelToInfo();
        Assertions.assertTrue(logCaptor.getInfoLogs().contains("Checking number: 123"));
        Assertions.assertTrue(logCaptor.getInfoLogs().contains("Result of checking number: 123"));
        Assertions.assertTrue(logCaptor.getInfoLogs().contains("Завершено: 123"));
    }

    @Test
    void testInvalidNumberInLogger() {
        NumberChecker numberChecker = new NumberChecker();

        logCaptor.setLogLevelToInfo();

        assertThrows(NumberFormatException.class, () -> {
            numberChecker.checkNumber("123Y");
        });
        Assertions.assertTrue(logCaptor.getErrorLogs().contains("For input string: \"123Y\""));
        Assertions.assertTrue(logCaptor.getInfoLogs().contains("Checking number: 123Y"));
        Assertions.assertTrue(logCaptor.getInfoLogs().contains("Завершено: 123Y"));
    }

}
