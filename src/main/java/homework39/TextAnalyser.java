package homework39;

import lesson39.JavaIOMusterStreamArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class TextAnalyser {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextAnalyser.class);


    public static void main(String[] args) {
        String textFilePath = "src/main/resources/text.txt";
        Map<Character, Integer> textToAnalyse= new HashMap<>();
        try (FileReader inputStream = new FileReader(textFilePath, StandardCharsets.UTF_8)){
            int data;
            while ((data = inputStream.read()) != -1) {
                char character = (char) data;
                textToAnalyse.put(character, textToAnalyse.getOrDefault(character, 0) + 1);
            }
        }
        catch (FileNotFoundException  exception) {
            LOGGER.error("Path: {} Exception: ", textFilePath,exception.getMessage());
        }
        catch (IOException exception) {
            LOGGER.error("Error open file. Exception: ", textFilePath,exception.getMessage());
        }
        textToAnalyse.forEach((character, count) -> {
            System.out.println(character + " - " + count);
        });
    }
}
