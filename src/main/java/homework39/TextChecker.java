package homework39;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TextChecker {
    public static void main(String[] args) {
        try(FileInputStream text1 = new FileInputStream("src/main/resources/text.txt");
            FileInputStream text2 = new FileInputStream("src/main/resources/textCopie.txt")){
            int data1, data2;
            int position = 0;
            boolean diffirenceFound = false;
            if(text1.available() <= text2.available()){
                while ((data1 = text1.read()) !=-1){
                    position++;
                    data2 = text2.read();
                    if(data1 != data2){
                        diffirenceFound = true;
                        System.out.println("Files are differten at position: " + position);
                    }
                }
            }
            else {
                while ((data2 = text2.read()) !=-1){
                    position++;
                    data1 = text1.read();
                    if(data1 != data2){
                        diffirenceFound = true;
                        System.out.println("Files are differten at position: " + position);
                    }
                }
            }


            if(!diffirenceFound && text1.available() == text2.available()){
                System.out.println("Files are identical.");
            }



        }catch (FileNotFoundException exception){
            System.out.println("File was not finding: " + exception.getMessage());
        }catch (IOException exception){
            System.out.println("Error reading file" + exception.getMessage());
        }
    }
}
