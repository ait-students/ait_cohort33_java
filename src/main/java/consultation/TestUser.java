package consultation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestUser {
    public static void main(String[] args) {
        User user =  new User("Alex", 20, "email@gmail.com");
        user.setAge(21);
        log.info(user.toString());
        System.out.println(user.toString());
    }
}
