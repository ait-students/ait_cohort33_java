package consultation;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ApachePOIExamples {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApachePOIExamples.class);

    public static void main(String[] args) {
        createSpreadSheet("students.xlsx", "Hello Students");
    }

    public static void createSpreadSheet(String fileName, String cellValue) {
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet("Students");
        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue(cellValue);
        Cell cell2 = row.createCell(1);
        cell2.setCellValue(2);
        Cell cell3 = row.createCell(2);
        cell3.setCellValue(3);
        Cell cell4 = row.createCell(3);
        cell4.setCellFormula("B1" + "+" + "C1");
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileName)) {
            wb.write(fileOutputStream);
        } catch (FileNotFoundException fileNotFoundException) {
            LOGGER.error("File not found", fileNotFoundException);
            throw new RuntimeException(fileNotFoundException);
        } catch (IOException ioException) {
            LOGGER.error("I/O error", ioException);
            throw new RuntimeException(ioException);
        } finally {
            if (wb != null) {
                try {
                    wb.close();
                } catch (IOException e) {
                    LOGGER.error("Error closing workbook", e);
                }
            }
        }
    }
}
