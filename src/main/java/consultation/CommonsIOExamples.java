package consultation;

import homework38.JavaTimeApiHomework;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class CommonsIOExamples {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonsIOExamples.class);


    public static void main(String[] args) {
        File directory = new File("testfolder");
        File testFile = new File("testfile.txt");
        File testFile2 = new File("testfile2.txt");
        try {
            FileUtils.forceMkdir(directory);
            LOGGER.info("Directory created");
            FileUtils.writeStringToFile(testFile,"Hello java\n","UTF-8",true);
            List<String> lines = FileUtils.readLines(testFile,"UTf-8");
            for (String line : lines) {
                System.out.println(line);
            }
            boolean result = FileUtils.contentEquals(testFile,testFile2);
            System.out.println(result);
            System.out.println("------------------------");
            try(LineIterator lineIterator = FileUtils.lineIterator(testFile,"UTF-8")) {
               while (lineIterator.hasNext()){
                   String line = lineIterator.nextLine();
                   System.out.println(line);
               }
            }
            IOUtils.copy(new FileInputStream(testFile), new FileOutputStream(testFile2));
            IOUtils.close();

            FileFilter fileFilter = FileFilterUtils.suffixFileFilter(".txt");
            File[] files = directory.listFiles(fileFilter);
            for (File file : files) {
                System.out.println(file.getName());
            }


           // FileUtils.deleteDirectory(directory);
           // LOGGER.info("Directory deleted");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
