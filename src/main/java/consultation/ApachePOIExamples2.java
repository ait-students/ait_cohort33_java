package consultation;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ApachePOIExamples2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApachePOIExamples2.class);

    public static void main(String[] args) {
        XWPFDocument document = new XWPFDocument();
            XWPFParagraph paragraph = document.createParagraph();
            paragraph.createRun().setText("Hello Word Document");
            try (FileOutputStream fileOutputStream = new FileOutputStream("wordFile.docx")){
                document.write(fileOutputStream);
                document.close();
            } catch (FileNotFoundException ex) {
                LOGGER.error(ex.getMessage());
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage());
            }
    }
}
