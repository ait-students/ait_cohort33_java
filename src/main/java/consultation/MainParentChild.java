package consultation;

import java.util.List;

public class MainParentChild {
    public static void main(String[] args) {
        Child child = new Child();
        Parent parent = new Child();
        //Parent parent1 = new Parent();
        Child.statMethod();
        Parent.statMethod();
        List<Integer> list = List.of(1,2,3);
    }
}
