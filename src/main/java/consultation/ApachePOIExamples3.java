package consultation;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ApachePOIExamples3 {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApachePOIExamples3.class);


    public static void main(String[] args) {
        XMLSlideShow xmlSlideShow = new XMLSlideShow();
        XSLFSlide slide = xmlSlideShow.createSlide();
        XSLFTextBox textBox = slide.createTextBox();
        textBox.setText("Hello Java");

        try(FileOutputStream fos = new FileOutputStream("test.pptx")) {
            xmlSlideShow.write(fos);
            xmlSlideShow.close();

        }
        catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
        catch(IOException e) {
            LOGGER.error(e.getMessage());
        }

    }
}
