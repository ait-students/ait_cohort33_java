package consultation;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString(exclude = "email")
public class User {

    private String name;

    private int age;

    private String email;

}
