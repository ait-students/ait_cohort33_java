package consultation;

import org.apache.commons.validator.routines.*;

public class CommonsValidatorExamples {
    public static void main(String[] args) {
        String emailValid = "test@test.com";
        String emailNoValid = "test.test.com";


        EmailValidator emailValidator = EmailValidator.getInstance();
        boolean result = emailValidator.isValid(emailNoValid);
        //System.out.println(result);

        IntegerValidator integerValidator = IntegerValidator.getInstance();
        int number = 150;
        String numberStr = "200";
        boolean resultNumber = integerValidator.isInRange(number, 100, 500);
        //System.out.println(resultNumber);
        //System.out.println(integerValidator.validate(numberStr));

        DateValidator dateValidator = DateValidator.getInstance();
        String dateStr = "2024-06-23";
        String dateStr2 = "20-01-2001";
        boolean resultDate = dateValidator.isValid(dateStr);
        //System.out.println("resultDate: "+ resultDate);

        UrlValidator urlValidator = UrlValidator.getInstance();
        boolean resultUrl = urlValidator.isValid("http://www.google.com");
        //System.out.println(resultUrl);

        IBANValidator ibanValidator = IBANValidator.getInstance();
        boolean resultIban = ibanValidator.isValid("GB82 1234 1234 5698 7654 32");
        //System.out.println(resultIban);

        DateValidator dateValidator2 = DateValidator.getInstance();
        String time = "22:30";
        boolean resultTime = dateValidator2.isValid(time, "HH:mm");
        //System.out.println(resultTime);

        DoubleValidator doubleValidator = DoubleValidator.getInstance();
        String doubleStr = "30.34";
        boolean resultDouble = doubleValidator.isValid(doubleStr);
        //System.out.println(resultDouble);

        RegexValidator plateValidator = new RegexValidator("^[A-Z]{2}-\\d{4}-[A-Z]{2}$");
        String plate1 = "AB-1234-CD";
        //System.out.println(plateValidator.isValid(plate1));

        RegexValidator passwordValidator = new RegexValidator("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
        String password1 = "Example$123";
        //System.out.println(passwordValidator.isValid(password1));

        ISBNValidator isbnValidator = ISBNValidator.getInstance();
        String isbn1 = "9783161484100";
        boolean resultISBN = isbnValidator.isValid(isbn1);
        System.out.println(resultISBN);

    }
}
