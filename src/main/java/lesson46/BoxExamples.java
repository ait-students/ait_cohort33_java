package lesson46;

public class BoxExamples {
    public static void main(String[] args) {
        Box<Integer> box = new Box<>();
        box.setItem(5554);
        Integer item = box.getItem();
        System.out.println(item);


        Box<String> boxString = new Box<>();
        boxString.setItem("Hallo my life");
        String myLife = boxString.getItem();
        System.out.println(myLife);

    }
}
