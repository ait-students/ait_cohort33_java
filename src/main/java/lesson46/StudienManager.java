package lesson46;

public class StudienManager {
    public static void main(String[] args) {
        Course<Student> matheCourse = new Course<>();
        matheCourse.addStudent(new Student("Musterman"));
        matheCourse.addStudent(new Student("Fritz"));

        System.out.append(matheCourse.toString());
    }
}
