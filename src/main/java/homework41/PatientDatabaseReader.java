package homework41;

import lesson41.PersonDeserializing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PatientDatabaseReader {


    private static final Logger LOGGER = LoggerFactory.getLogger(PatientDatabaseReader.class);


    private static final String FILE_PATH = "src/main/resources/patients_database.txt";

    public static void main(String[] args) {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(FILE_PATH))){
            String line;
            //ФИО, возраст, диагноз, дата последнего визита
            while ((line = bufferedReader.readLine()) != null) {
                //LOGGER.info(line);
                String[] patientDataArray = line.split(", ");
                LOGGER.info("ФИО: {}, возраст: {}, диагноз: {}, дата последнего визита: {}", patientDataArray[0], patientDataArray[1], patientDataArray[2], patientDataArray[3] );
            }
        }
        catch (FileNotFoundException exception){
           LOGGER.error(exception.getMessage());
        }
        catch (IOException exception){
            LOGGER.error(exception.getMessage());
        }
    }
}
