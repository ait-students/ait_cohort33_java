package homework41;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DeserializePatient {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeserializePatient.class);
    private static final String FILE_PATH = "src/main/resources/patients.txt";

    public static void main(String[] args) {
        try (FileInputStream fileInputStream = new FileInputStream(FILE_PATH);
             ObjectInputStream objectOutputStream = new ObjectInputStream(fileInputStream)) {

            List<Patient> patients = (ArrayList<Patient>) objectOutputStream.readObject();
            patients.forEach(System.out::println);
        } catch (FileNotFoundException e) {
            LOGGER.error("Проблема с доступом в файл или файл не найден");
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (ClassNotFoundException e) {
            LOGGER.error("Проблема с десериализацией. Проверьте версии");
            LOGGER.error(e.getMessage());
        }
    }
}
