package homework41;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class MedicalAnalysisWriter {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicalAnalysisWriter.class);
    private static final String FILE_PATH = "src/main/resources/analysis_database.txt";

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String answer = "";
        do {
            System.out.println("ФИО пациента:");
            String name = scanner.nextLine();
            System.out.println("тип анализа:");
            String analyseType = scanner.nextLine();
            System.out.println("значение анализа:");
            String analyseResult = scanner.nextLine();
            System.out.println("дата проведения анализа:");
            String analyseDate = scanner.nextLine();

            String data = name + ", " + analyseType + ", " + analyseResult + ", " + analyseDate;

            try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE_PATH, true))) {
                bufferedWriter.write(data);
                bufferedWriter.newLine();
                System.out.println("Еще будут записи? (Y/N)");
                answer = scanner.nextLine();
            }

            catch ( IOException ioException) {
                LOGGER.error(ioException.getMessage());
            }
        }
        while (answer.equalsIgnoreCase("y"));

    }
}
