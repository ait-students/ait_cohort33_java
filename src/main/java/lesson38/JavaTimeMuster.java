package lesson38;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class JavaTimeMuster {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.of(2024,05,20);
        System.out.println(localDate);
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
        LocalTime localTimeNew = LocalTime.of(13,45);
        System.out.println(localTimeNew);
        LocalDateTime localDateTime = LocalDateTime.of(2024,9,12,20,00,39);
        System.out.println(localDateTime);

        LocalDateTime localDateTime1New = LocalDateTime.of(2024,1,1,12,45);
        ZoneOffset zoneOffset = ZoneOffset.of("+02:00");
        OffsetDateTime offsetDateTime = OffsetDateTime.of(localDateTime1New,zoneOffset);
        System.out.println(offsetDateTime);

        LocalDateTime localDateTime3 = LocalDateTime.of(2024,6,4,20,30,39);
        ZoneId zoneId = ZoneId.of("Asia/Tokyo");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime3,zoneId);
        System.out.println(zonedDateTime);

        MonthDay monthDay = MonthDay.of(Month.APRIL, 20);
        System.out.println(monthDay);
        YearMonth yearMonth = YearMonth.of(2024, Month.APRIL);
        System.out.println(yearMonth);
        Year year = Year.of(19000050);
        System.out.println(year);
        System.out.println("------------------------");

        LocalDate localDate4 = LocalDate.of(2024,01,01);
        System.out.println(localDate4);
        localDate4 = localDate4.plusDays(24);
        System.out.println(localDate4);
        localDate4 = localDate4.plusDays(36);
        System.out.println(localDate4);
        localDate4 = localDate4.minusDays(20);
        System.out.println(localDate4);

        if(LocalDate.now().isAfter(localDate4)){
            System.out.println("is after now");
        }
        else {
            System.out.println("is not after now");
        }

        LocalDate date = LocalDate.parse("2024-02-02");
        System.out.println(date);
        LocalDateTime localDateTimeToParse = LocalDateTime.parse("2024-02-02T12:30:23");
        System.out.println(localDateTimeToParse);

        String format = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
                .withLocale(Locale.GERMAN).format(LocalDate.of(2024, 04, 01));
        System.out.println(format);

        LocalDate startLocalDate = LocalDate.parse("2024-02-02");
        LocalDate endLocalDate = LocalDate.parse("2024-12-02");
        long until = startLocalDate.until(endLocalDate, ChronoUnit.WEEKS);
        System.out.println(until);


    }
}
