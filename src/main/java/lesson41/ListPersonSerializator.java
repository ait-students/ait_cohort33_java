package lesson41;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;

public class ListPersonSerializator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListPersonSerializator.class);


    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<>();
        people.add(new Person("Mustermann", 32, true, "adress:Berlin", "3211"));
        people.add(new Person("Anna.Lena", 12, false, "adress:Nürnberg", "1122"));

        try(ObjectOutputStream objectOutputStream =
                    new ObjectOutputStream(new FileOutputStream("src/main/resources/people.txt"));
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("src/main/resources/people.txt"))) {
            objectOutputStream.writeObject(people);

            ArrayList<Person> deserializedPerson = ( ArrayList<Person>) objectInputStream.readObject();
            for (Person person : deserializedPerson){
                System.out.println(person);
            }
        } catch (FileNotFoundException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }



    }
}
