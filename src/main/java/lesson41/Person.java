package lesson41;

import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 2L;

    private String name;
    private int age;
    private boolean education;
    private String adress;
    private transient String password;

    public Person(String name, int age, boolean education, String adress, String password) {
        this.name = name;
        this.age = age;
        this.education = education;
        this.adress = adress;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", education=" + education +
                ", adress='" + adress + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
