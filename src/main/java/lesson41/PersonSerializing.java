package lesson41;

import lesson40.JavaIOByteOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class PersonSerializing {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonSerializing.class);


    public static void main(String[] args) {
        Person person =  new Person("Jackie Chan", 70, true, "adress..", "123");

        try(FileOutputStream fileOutputStream = new FileOutputStream("/Users/entwickler/Desktop/person.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)){
            objectOutputStream.writeObject(person);
            LOGGER.info("Person serialized successfully");

        }
        catch (FileNotFoundException exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
        catch (IOException exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
    }
}
