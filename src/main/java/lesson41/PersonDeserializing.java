package lesson41;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class PersonDeserializing {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDeserializing.class);

    public static void main(String[] args) {
        try (FileInputStream fileInputStream = new FileInputStream("/Users/entwickler/Desktop/person.ser");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            Person personJackieChan = (Person) objectInputStream.readObject();
            LOGGER.info("personJackieChan: {}", personJackieChan);

        } catch (FileNotFoundException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (IOException exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
        catch (ClassNotFoundException exception) {
            LOGGER.error(exception.getMessage(), exception);
        }


    }

}
