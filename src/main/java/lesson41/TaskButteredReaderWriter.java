package lesson41;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class TaskButteredReaderWriter {
    /*
    Напишите программу, которая считывает содержимое текстового файла input.txt
     и записывает его в файл output.txt с
     использованием буферизированных потоков (BufferedReader и BufferedWriter).
     */

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskButteredReaderWriter.class);

    public static void main(String[] args) {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("src/main/resources/input.txt"));
            BufferedWriter bufferedWriter =     new BufferedWriter(new FileWriter("src/main/resources/output.txt"))){
            String line;
            while((line = bufferedReader.readLine()) != null){
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
        }

        catch (FileNotFoundException exception){
            LOGGER.error(exception.getMessage());
        }
        catch (IOException exception){
            LOGGER.error(exception.getMessage());
        }
    }
}
