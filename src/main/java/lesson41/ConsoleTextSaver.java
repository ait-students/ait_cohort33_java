package lesson41;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;

public class ConsoleTextSaver {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleTextSaver.class);

    /*
    Напишите программу, которая считывает строки с консоли
    до ввода слова "exit" и записывает их в файл output.txt.
     */
    public static void main(String[] args) {
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            FileWriter fileWriter = new FileWriter("src/main/resources/output.txt")){
            System.out.println("Введите текст или exit для завершения");
            String line;
            while ((!(line = bufferedReader.readLine()).equalsIgnoreCase("exit"))){
                fileWriter.write(line + "\r\n");
            }
            LOGGER.info("Ввод с консоли успешно сохранен в файл");
        }
        catch (IOException exception){
            LOGGER.error(exception.getMessage());

        }
    }
}
