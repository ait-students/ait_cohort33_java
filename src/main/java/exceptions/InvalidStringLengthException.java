package exceptions;

public class InvalidStringLengthException extends Throwable {
    public InvalidStringLengthException(String message) {
        super(message);
    }
}
