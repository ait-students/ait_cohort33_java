package lesson45;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JavaStreamApi3 {

    private static List<String> carList = new ArrayList<>();


    public static void main(String[] args) {
        carList.add("Audi");
        carList.add("BMW");
        carList.add("Golf");
        carList.add("Honda");
        carList.add("Ford");
        carList.add("Mazda");

        System.out.println(carList.stream().count());

        List<Integer> elementsInt = Arrays.asList(1,2,3,4,5,6,7,8,9);
        Optional<Integer> sum = elementsInt.stream().reduce((y, x) -> y + x);
        System.out.println(sum.get());

        Optional <Integer> resultMinElement = elementsInt.stream().min(Integer::compareTo);
        System.out.println(resultMinElement.get());

        Optional <Integer> resultFinstFirst = elementsInt.stream().filter(x-> x > 5).findFirst();
        System.out.println(resultFinstFirst.get());


        //Найдите минимальное значение в списке целых чисел.
        List<Integer> numbers = Arrays.asList(3, 1, 4, 1, 5, 9, 12);
        Optional<Integer> min = numbers.stream().min(Integer::compareTo);
        System.out.println(min.get());

        //Найдите минимальное значение в списке целых чисел.
        Optional<Integer> max = numbers.stream().max(Integer::compareTo);
        System.out.println(max.get());

        //Подсчитайте количество элементов в списке.
        long count = numbers.stream().count();
        System.out.println(count);

        //Пропустите первые два элемента в списке и выведите оставшиеся.
        numbers.stream().skip(2).forEach(System.out::print);
        System.out.println("-----");

        //Возьмите только первые три элемента из списка.
        numbers.stream().limit(3).forEach(System.out::print);

        //Отсортируйте список строк в алфавитном порядке.
        List<String> collect = carList.stream().sorted().collect(Collectors.toList());
        System.out.println(collect);

        //Проверьте, все ли числа в списке положительные.
        boolean match = numbers.stream().allMatch(x -> x > 0);
        System.out.println(match);

        //Проверьте, есть ли в списке хотя бы одно четное число.
        boolean anyMatch = numbers.stream().anyMatch(x -> x % 2 == 0);
        System.out.println(anyMatch);

        //Найдите первое четное число в списке.
        Optional<Integer> first = numbers.stream().filter(x -> x % 2 == 0).findFirst();
        System.out.println(first.get());

        //Объедините два списка строк в один поток и выведите все элементы.
        List<String> list1 = Arrays.asList("apple", "banana");
        List<String> list2 = Arrays.asList("cherry", "date");
        Stream.concat(list1.stream(), list2.stream()).forEach(System.out::println);

        //Разделите список строк на два списка: один с длиной строк меньше 5, другой — с длиной 5 и больше.
        List<String> words = Arrays.asList("apple", "banana", "fig", "date");
        Map<Boolean, List<String>> collected = words.stream().collect(Collectors.partitioningBy(word -> word.length() < 5));
        System.out.println(collected);
    }
}
