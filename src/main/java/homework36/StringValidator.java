package homework36;

import exceptions.InvalidStringLengthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(StringValidator.class);


    public void validateStringLength(String stringToValidate) throws InvalidStringLengthException {
        LOGGER.info("Validating string length: " + stringToValidate);
        if(stringToValidate.length() < 5){
            LOGGER.error("Invalid string length");
            throw new InvalidStringLengthException("String lenth is less than 5");
        }
            LOGGER.info("String length is greater than 5 = "+ stringToValidate.length());
    }
}
