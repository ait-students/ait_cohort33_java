package homework36;

import exceptions.InvalidStringLengthException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) {
        NumberChecker numberChecker =  new NumberChecker();
        StringValidator stringValidator =  new StringValidator();
        ExceptionPropagation exceptionPropagation =  new ExceptionPropagation();
        try {
            numberChecker.checkNumber("123");
            //numberChecker.checkNumber2("44562");
            //stringValidator.validateStringLength("12342");
            //exceptionPropagation.method1();
        }
       /* catch (NumberFormatException | IOException | InvalidStringLengthException exception){
            LOGGER.error(exception.getMessage());
        }*/
        finally {
            LOGGER.info("End of program");
        }

    }
}
