package lesson40;

import lesson39.JavaIOMuster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class JavaIOOutput {

    private static final Logger LOGGER = LoggerFactory.getLogger(JavaIOOutput.class);


    public static void main(String[] args) {
        String text = "Java IO Hello";

        try (FileOutputStream fileOutputStream = new FileOutputStream("output.txt")) {
            byte[] bytes = text.getBytes();
            //запись массива байтов
            fileOutputStream.write(bytes,0,bytes.length);
            //Запись одиночного байта
            fileOutputStream.write(bytes[0]);
            LOGGER.info("Written output to file: {}", text);
        }
        catch (FileNotFoundException exception) {
           LOGGER.error(exception.getMessage(), exception);
        }
        catch (IOException exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
    }
}
