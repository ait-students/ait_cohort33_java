package lesson40;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class JavaIOByteOutputStream {
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaIOByteOutputStream.class);


    public static void main(String[] args) {
        String test = "Hello Java 2024!!!";
        byte[] bytes = test.getBytes();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byteArrayOutputStream.write(bytes);
        }
        catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        finally {
            if(byteArrayOutputStream != null) {
                try {
                    //byteArrayOutputStream.close();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

        }
        System.out.println(byteArrayOutputStream.toByteArray());
        for (byte b : byteArrayOutputStream.toByteArray()) {
            System.out.println( (char) b);
        }

        try (FileOutputStream fileOutputStream = new FileOutputStream("fileOutput.txt");
             FileOutputStream fileOutputStream2 = new FileOutputStream("fileOutput2.txt")){
            byteArrayOutputStream.writeTo(fileOutputStream);
            byteArrayOutputStream.writeTo(fileOutputStream2);
            byteArrayOutputStream.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
