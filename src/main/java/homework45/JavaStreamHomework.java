package homework45;

import java.util.*;
import java.util.stream.Collectors;

public class JavaStreamHomework {
    public static void main(String[] args) {
        //Используя Stream API, выведите на экран все строки из списка.
        List<String> fruits = Arrays.asList("apple", "banana", "cherry");
        fruits.stream().forEach(System.out::println);

        //Используя Stream API, преобразуйте все строки из списка в верхний регистр и соберите результат в новый список.
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
        List<String> collect =names.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(collect);

        //Используя Stream API, подсчитайте количество строк в списке, которые содержат более 4 символов.
        List<String> words = Arrays.asList("stream", "api", "java", "list");
        long countWords = words.stream().filter(x-> x.length() > 4).count();
        System.out.println("countWords: " + countWords);

        //Используя Stream API, найдите первый элемент в списке, который начинается на букву 'j'.
        List<String> tools = Arrays.asList("hammer", "jack", "wrench", "johny");
        String firstJElement = tools.stream().filter(x -> x.startsWith("j")).findFirst().orElse(null);
        System.out.println("firstJElement: " + firstJElement);

        //Отфильтруйте список чисел, оставив только те, которые больше 10, и затем отсортируйте оставшиеся элементы.
        List<Integer> numbers = Arrays.asList(5, 13, 1, 21, 8);
        List<Integer> filteredNumbers = numbers.stream().filter(x -> x > 10).sorted().collect(Collectors.toList());
        System.out.println("filteredNumbers: " + filteredNumbers);

        // Используя Stream API, сгруппируйте строки из списка по начальной букве.
        List<String> animals = Arrays.asList("ant", "ant", "bear", "alligator");
        Map<Character, List<String>> animalsGroups = animals.stream().collect(Collectors.groupingBy(x -> x.charAt(0)));
        animalsGroups.forEach((key,value) -> System.out.println(key + ":" + value));
        System.out.println("animalsGroups: " + animalsGroups);

        //Используя Stream API, найдите сумму всех чисел в списке.
        List<Integer> values = Arrays.asList(2, 3, 5, 7, 11);
        long valuesSum = values.stream().mapToLong(Integer::intValue).sum();
        System.out.println("valuesSum: " + valuesSum);

        //Используя Stream API, получите статистику (минимум, максимум) по списку чисел.
        List<Integer> stats = Arrays.asList(10, 20, 30, 40, 50);
        int minResult = stats.stream().min(Integer::compareTo).orElse(null);
        System.out.println("minResult: " + minResult);
        int maxResult = stats.stream().max(Integer::compareTo).orElse(null);
        System.out.println("maxResult: " + maxResult);

        IntSummaryStatistics intSummaryStatistics = stats.stream().mapToInt(Integer::intValue).summaryStatistics();
        System.out.println("intSummaryStatistics: " + intSummaryStatistics);

        //Используя Stream API, объедините все строки из списка в одну строку, разделяя их пробелом.
        List<String> phrases = Arrays.asList("Java", "Stream", "API");
        String phrasesLine =  phrases.stream().collect(Collectors.joining(" "));
        System.out.println("phrasesLine: " + phrasesLine);

        //Используя Stream API, рассчитайте общую длину всех строк в списке.
        List<String> messages = Arrays.asList("hello", "world", "streams", "are", "cool");
        long messagesLength = messages.stream().mapToInt(String::length).sum();
        System.out.println("messagesLength: " + messagesLength);

        long messagesLineLength =  messages.stream().collect(Collectors.joining()).length();
        System.out.println("messagesLineLength: " + messagesLineLength);




    }
}
