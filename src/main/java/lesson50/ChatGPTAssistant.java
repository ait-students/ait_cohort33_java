package lesson50;

import javax.json.Json;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ChatGPTAssistant {

    public static void main(String[] args) {
        String userPrompt = "Java что такое?";
        String apiKey = "keykeykey";
        String chatGPTResponse = generateChatGPTResponse(userPrompt, apiKey);
        System.out.println(chatGPTResponse);
    }

    public static String generateChatGPTResponse(String userPrompt, String apiKey) {
        String apiURL = "https://api.openai.com/v1/chat/completions";
        String llmname = "gpt-4o";
        try {
            // Create URL object
            URL url = new URL(apiURL);
            // Open connection
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // Set the request method to POST
            connection.setRequestMethod("POST");
            // Set request headers
            connection.setRequestProperty("Authorization", "Bearer " + apiKey);
            connection.setRequestProperty("Content-Type", "application/json");
            // Create the request body
            String requestBody = Json.createObjectBuilder()
                    .add("model", llmname)
                    .add("messages", Json.createArrayBuilder()
                            .add(Json.createObjectBuilder()
                                    .add("role", "user")
                                    .add("content", userPrompt)))
                    .build()
                    .toString();
            // Enable input/output streams
            connection.setDoOutput(true);
            // Write the request body
            try (OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream())) {
                writer.write(requestBody);
                writer.flush();
            }
            // Read the response
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                return getLLMResponse(response.toString());
            }
        }
        catch (IOException e){
            return null;
        }
    }

    private static String getLLMResponse(String response) {
        int firstChar = response.indexOf("content") + 11;
        int lastChar = response.indexOf("\"", firstChar);
        if (firstChar == 10 || lastChar == -1) {
            // Log warning here, for example:
            // log.warn("Unexpected response format");
            return null;
        }
        return response.substring(firstChar, lastChar);
    }

}
