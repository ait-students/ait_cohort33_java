package lesson50;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NoteApplication {
    private static List<String> notes = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("1. Add Note");
            System.out.println("2. Remove Note");
            System.out.println("3. Show All Notes");
            System.out.println("4. Exit");

            String choice = scanner.nextLine();


            switch (choice) {
                case "1":
                    System.out.println("Enter your note:");
                    String note = scanner.nextLine();
                    notes.add(note);
                    System.out.println("Note added successfully!");
                    break;
                case "2":
                    System.out.println("Enter note index to remove (0-based):");
                    int index = scanner.nextInt();
                    scanner.nextLine();
                    if (index >= 0 && index < notes.size()) {
                        notes.remove(index);
                        System.out.println("Note removed successfully!");
                    } else {
                        System.out.println("Invalid index!");
                    }
                    break;
                case "3":
                    System.out.println("All notes:");
                    for (int i = 0; i < notes.size(); i++) {
                        System.out.println((i + 1) + ". " + notes.get(i));
                    }
                    break;
                case "4":
                    System.out.println("Exiting...");
                    scanner.close();
                    return;
                default:
                    System.out.println("Invalid choice, please try again.");
            }
        }
    }
}
