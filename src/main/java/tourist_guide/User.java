package tourist_guide;

public class User {
    private String name;
    private int age;
    private String preferences;

    public User(String name, int age, String preferences) {
        this.name = name;
        this.age = age;
        this.preferences = preferences;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }

    @Override
    public String toString() {
        return "User: " + name + ", age: " + age + ", preferences: " + preferences;
    }
}
