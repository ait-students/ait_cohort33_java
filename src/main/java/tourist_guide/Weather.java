package tourist_guide;

public class Weather {
    private double temperature;
    private String humidity;
    private double precipitationChance;

    public Weather(double temperature, String humidity, double precipitationChance) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.precipitationChance = precipitationChance;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }


    public double getPrecipitationChance() {
        return precipitationChance;
    }

    public void setPrecipitationChance(double precipitationChance) {
        this.precipitationChance = precipitationChance;
    }

    @Override
    public String toString() {
        return "Погода: " + humidity + ", Температура: " + temperature + "°C, Вероятность осадков: " + precipitationChance + "%";
    }
}
