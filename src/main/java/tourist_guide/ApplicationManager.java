package tourist_guide;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ApplicationManager {

    private static Scanner scanner = new Scanner(System.in);

    private static List<TouristSpot> spots = new ArrayList<>();

    public static void main(String[] args) {
        initSpotsFromFile();
        boolean stopper = true;
        while (stopper) {
            System.out.println("Добро пожаловать в приложение \"Путеводитель\"!\n" +
                    "Пожалуйста, выберите действие:\n" +
                    "1. Добавить новое туристическое место\n" +
                    "2. Поиск по категории\n" +
                    "3. Показать все места\n" +
                    "4. Обновить существующее место\n" +
                    "5. Удалить место\n" +
                    "6. Выход\n" +
                    "Введите ваш выбор: ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    addNewTouristSpot();
                    break;
                case 2:
                    searchByCategory();
                    break;
                case 3:
                    showAllSpots();
                    break;
                case 4:
                    renewSpot();
                    break;
                case 5:
                    deleteSpot();
                    break;
                case 6:
                    stopper = false;
                    exit();
                    break;
                default:
                    System.out.println("Неверный ввод, введите корректное значение");
            }
        }

    }

    //choice 1
    public static void addNewTouristSpot() {
        System.out.println("Вы выбрали добавление нового туристического места.Пожалуйста, введите следующие данные:");
        System.out.print("Название: ");
        String spotName = scanner.nextLine();
        System.out.print("Описание: ");
        String spotDescription = scanner.nextLine();
        System.out.println("Категория: (PARK,MUSEUM)");
        String spotCategoryStr = scanner.nextLine();
        SpotCategories spotCategories = SpotCategories.valueOf(spotCategoryStr);
        System.out.println("Рейтинг (от 1 до 5)");
        int spotRating = scanner.nextInt();
        System.out.println("Стоимость: ");
        int costVisitSpot = scanner.nextInt();

        TouristSpot touristSpot = new TouristSpot(spotName, spotDescription, spotCategories, spotRating, costVisitSpot);
        spots.add(touristSpot);

        System.out.println("Новое туристическое место успешно добавлено!");
        System.out.println(touristSpot.toString());

    }

    //choice 2
    public static void searchByCategory() {
        System.out.println("Вы выбрали поиск по категории.");
        System.out.println("Доступные категории: PARK,MUSEUM");
        System.out.print("Введите категорию: ");
        String spotCategoryStr = scanner.nextLine();
        SpotCategories spotCategories = SpotCategories.valueOf(spotCategoryStr);

        List<TouristSpot> filteredSpots = spots.stream()
                .filter(s -> s.getSpotCategorie().equals(spotCategories))
                .collect(Collectors.toList());
        System.out.println("Найдено: " + filteredSpots.size() + " мест в категории: ");
        filteredSpots.forEach(System.out::println);
    }

    public static void showAllSpots() {
        if (spots.isEmpty()) {
            System.out.println("В списке не внесено ни одного туристического места");
        } else {
            for (TouristSpot element : spots) {
                System.out.println(element.toString());
            }
        }
    }

    public static void renewSpot() {
        System.out.print("Введите название места для обновления: ");
        String spotName = scanner.nextLine();

        boolean found = false;
        for (TouristSpot spot : spots) {
            if (spot.getSpotName().equalsIgnoreCase(spotName)) {
                System.out.println("Текущая информация о месте:");
                System.out.println(spot.toString());

                System.out.print("Введите новую стоимость: ");
                int newCost = scanner.nextInt();
                scanner.nextLine();

                spot.setCostVisitSpot(newCost);
                System.out.println("Туристическое место успешно обновлено!");
                System.out.println(spot.toString());

                found = true;
                break;
            }
        }
        if (!found) {
            System.out.println(spotName + " не найден !!!");
        }
    }


    public static void deleteSpot() {
        System.out.println("Введите название места, которое вы хотите удалить:");
        String currentSpot = scanner.nextLine();
        Iterator<TouristSpot> iterator = spots.iterator();
        while (iterator.hasNext()) {
            TouristSpot spot = iterator.next();
            if (currentSpot.equals(spot.getSpotName())) {
                iterator.remove();
                System.out.println(currentSpot + " был успешно удален");
            }
        }
    }

    public static void exit() {
        try (ObjectOutputStream objectOutputStream =
                     new ObjectOutputStream(new FileOutputStream("spots.txt"))) {
            objectOutputStream.writeObject(spots);
            System.out.println("Информация сохранена в файл!");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static void initSpotsFromFile() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("spots.txt"))) {
            ArrayList<TouristSpot> touristSpots = (ArrayList<TouristSpot>) objectInputStream.readObject();
            spots = new ArrayList<>(touristSpots);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
