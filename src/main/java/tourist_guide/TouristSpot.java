package tourist_guide;

import java.io.Serializable;
import java.util.Objects;

public class TouristSpot implements Serializable {

    private static final long serialVersionUID = 1L;

    private String spotName;
    private String spotDescription;
    private SpotCategories spotCategorie;
    private int spotRating;
    private int costVisitSpot;

    public TouristSpot(String spotName, String spotDescription, SpotCategories spotCategorie, int spotRating, int costVisitSpot) {
        this.spotName = spotName;
        this.spotDescription = spotDescription;
        this.spotCategorie = spotCategorie;
        this.spotRating = spotRating;
        this.costVisitSpot = costVisitSpot;
    }

    public String getSpotName() {
        return spotName;
    }

    public void setSpotName(String spotName) {
        this.spotName = spotName;
    }

    public String getSpotDescription() {
        return spotDescription;
    }

    public void setSpotDescription(String spotDescription) {
        this.spotDescription = spotDescription;
    }

    public SpotCategories getSpotCategorie() {
        return spotCategorie;
    }


    public int getSpotRating() {
        return spotRating;
    }

    public void setSpotRating(int spotRating) {
        this.spotRating = spotRating;
    }

    public int getCostVisitSpot() {
        return costVisitSpot;
    }

    public void setCostVisitSpot(int costVisitSpot) {
        this.costVisitSpot = costVisitSpot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TouristSpot that = (TouristSpot) o;
        return spotRating == that.spotRating && costVisitSpot == that.costVisitSpot && Objects.equals(spotName, that.spotName) && Objects.equals(spotDescription, that.spotDescription) && Objects.equals(spotCategorie, that.spotCategorie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(spotName, spotDescription, spotCategorie, spotRating, costVisitSpot);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Туристическое место: ");
        sb.append(", Стоимость: '").append(costVisitSpot).append('\'');
        sb.append(", Описание: '").append(spotDescription).append('\'');
        sb.append(", Название: '").append(spotName).append('\'');
        sb.append(", Рейтинг: ").append(spotRating);
        return sb.toString();


    }


}
